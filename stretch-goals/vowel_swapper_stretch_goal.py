
def vowel_swapper(string):
    # ==============
    # Your code here
    #
    #This probably isn't the best way to do this but it works.
    #
    #Split string into a list so that I can check and replace each letter individually
    def string_splitter(LsString) :
        return [char for char in LsString]
    
    LsString = string_splitter(string)
    #Set variable to count how many of each letter has been obsorvered by the loop.
    A = 0
    E = 0
    I = 0
    O = 0
    U = 0
    
    for index, item in enumerate(LsString):
        #Check if the character is one of the characters we're looking for
        if (LsString[index] == "a") or (LsString[index] == "A"):
            #check if the character has been obsereved exactly once before this instance.
            if A == 1:
                #If true this character must be the second instance and needs to be replaced.
                LsString[index] = "4"
                #Increment the variable
                A = A +1
            else:
                A = A + 1
                
        if (LsString[index] == "e") or (LsString[index] == "E"):
            if E == 1:
                LsString[index] = "3"
                E = E + 1
            else:
                E = E + 1
                
        if (LsString[index] == "i") or (LsString[index] == "I"):
            if I == 1:
                LsString[index] = "!"
                I = I + 1
            else:
                I = I + 1
                
        if (LsString[index] == "o") or (LsString[index] == "O"):
            if O == 1:
                if LsString[index] == "o":
                    LsString[index] = "ooo"
                elif LsString[index] == "O":
                    LsString[index] = "000"
                O = O + 1
            else:
                O = O + 1
                
        if (LsString[index] == "u") or (LsString[index] == "U"):
            if U == 1:
                LsString[index] = "|_|"
                U = U + 1
            else:
                U = U + 1
    #join the list back into a string with nothing seperating each entry in the list
    y = "".join(LsString)
    #return list
    return y
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
