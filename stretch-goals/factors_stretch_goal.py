def factors(number):
    # ==============
    Results = []
    for i in range(2, int(number)):
        f = int(number)%int(i)
        if f == 0:
            Results.append(i)
    if len(Results)== 0:
        Results = str(number) + " is a prime number"
    return Results
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number.”
