def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        result = int(a)+int(b)
    elif operator == "-":
        result = int(a)-int(b)
    elif operator == "*":
        result = int(a)*int(b)
    elif operator == "/":
        result = int(int(a)/int(b))
    Binary = ""
    while int(result)> 0:
        if result%2 == 0:
            Binary = "0" + Binary
        else:
            Binary = "1" + Binary
        result = int(int(result)/2)
    return Binary
    
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
